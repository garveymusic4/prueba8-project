const startButton = document.getElementById('start-btn')
const nextButton = document.getElementById('next-btn')
const questionContainerElement = document.getElementById('question-container')
const questionElement = document.getElementById('question3')
// const imageElement3 = document.getElementById('img3')
// const lastImage = document.querySelector('#image_X');
const answerButtonsElement = document.getElementById('answer-buttons')
let scoreIndex = 0;
let newLine = document.createElement('p');

let shuffledQuestions, shuffledImage, currentQuestionIndex, currentImageIndex


startButton.addEventListener('click', startGame)
nextButton.addEventListener('click', () => {
    
    currentQuestionIndex++
    
    // document.body.lastImage.style.visibility = "none"
    // currentImageIndex++
    setNextQuestion()
})

function startGame() {
    startButton.classList.add('hide')
    // questionContainerElement.classList('destroy').remove
    newLine.remove()
    shuffledQuestions = questions
    // shuffledImage = questions.image

    currentQuestionIndex = 0
    // currentImageIndex = 0
    questionContainerElement.classList.remove('hide')
    setNextQuestion()
}

function setNextQuestion() {
    resetState()
    // document.clearStatusClass.remove(lastImage)
    showQuestion(shuffledQuestions[currentQuestionIndex])
    // lastImage.remove(questions["image"])
    document.images[0].remove()
    // document.body.remove()

    // document.body.removeChild(imgArray);

    // showImage(shuffledImage[currentImageIndex])
    // display_image()
}

function showQuestion(question3) {
    questionElement.innerText = question3.question
    var myimages = document.body.append(question3.image)





    // lastImage.innerText = question3.image

    // myimages.pop();
    // // myimages.removeChild(myimages.imgArray);
    // imageELement.appendChild() = question3.image

    question3.answers.forEach(answer => {
        const button = document.createElement('button')
        button.innerText = answer.text
        button.classList.add('btn')
        if (answer.correct) {

            button.dataset.correct = answer.correct

        }
        button.addEventListener('click', selectAnswer)
        answerButtonsElement.appendChild(button)

    })
}

// function display_image(img3) {
//     imageElement3.appendChild() = img3.image
// }

function resetState() {
    clearStatusClass(document.body)
    // document.body.remove(question3["image   "])

    // clearStatusClass(document.images)
    // imageElement3.removeChild(currentImageIndex)
    nextButton.classList.add('hide')
    




    while (answerButtonsElement.firstChild) {
        answerButtonsElement.removeChild(answerButtonsElement.firstChild)

    }
}



function selectAnswer(e) {
    const selectedButton = e.target
    selectedButton.style.border = "medium solid orange"
    const correct = selectedButton.dataset.correct
    if (correct) {
        scoreIndex++;
    }
    setStatusClass(document.body, correct)
    Array.from(answerButtonsElement.children).forEach(button => {
        setStatusClass(button, button.dataset.correct)

    })
    if (shuffledQuestions.length > currentQuestionIndex + 1) {
        nextButton.classList.remove('hide')
        // image_x.parentNode.removeChild(image_x);

    }

    else {
        // console.log("Thank you for taking the quiz!")
        const finalMessage = "Well done, your score is " + scoreIndex + " out of 33";

        startButton.innerText = 'Restart'
        startButton.classList.remove('hide')
        // nextButton.className = "next-btn btn"
        
        newLine.className = "destroy";
        questionContainerElement.appendChild(newLine)
        newLine.innerText = finalMessage;
       
    }
}

function setStatusClass(element, correct) {
    clearStatusClass(element)
    if (correct) {
        element.classList.add('correct')





    } else {
        element.classList.add('wrong')
    }
}

function clearStatusClass(element) {
    element.classList.remove('correct')

    element.classList.remove('wrong')
    // document.body.removeChild(myimages)
    // element.currentImageIndex.remove("wrong")
}

var imgArray = new Array();


imgArray[0] = new Image();
imgArray[0].src = 'img/redsetter.jpg';

imgArray[1] = new Image();
imgArray[1].src = 'img/networknoel.jpg';

imgArray[2] = new Image();

imgArray[2].src = 'img/longroutes.png';

imgArray[3] = new Image();
imgArray[3].src = 'img/volkswagen.jpg';

imgArray[4] = new Image();
imgArray[4].src = 'img/hoakwagen.png';


imgArray[5] = new Image();
imgArray[5].src = 'img/resized.png';


imgArray[6] = new Image();
imgArray[6].src = 'img/goahead.jpg';
// 'img/resized.png'; 


imgArray[7] = new Image();
imgArray[7].src = 'img/orbitalls.png';

imgArray[8] = new Image();
imgArray[8].src = 'img/railfunding.jpg';


imgArray[9] = new Image();
imgArray[9].src = 'img/buslanem50.jpg';

// 

imgArray[10] = new Image();
imgArray[10].src = 'img/UCC.jpg';

imgArray[11] = new Image();
imgArray[11].src = 'img/activetravel2.png';

imgArray[12] = new Image();
imgArray[12].src = 'img/silentkiller.jpg';

imgArray[13] = new Image();
imgArray[13].src = 'img/bodyeffects.png';

imgArray[14] = new Image();
imgArray[14].src = 'img/hyundaicorrupt.png';

imgArray[15] = new Image();
imgArray[15].src = 'img/SDCC.png';

// oconnellstreet2.jpg
imgArray[16] = new Image();
imgArray[16].src = 'img/latelate.jpg';

imgArray[17] = new Image();
imgArray[17].src = 'img/oconnellstreet2.jpg';
// Aircoach1.jpg
imgArray[18] = new Image();
imgArray[18].src = 'img/wetcity.png';

imgArray[19] = new Image();
imgArray[19].src = 'img/copenhagen.png';

imgArray[20] = new Image();
imgArray[20].src = 'img/FXDpyvwWAAAY6SH.jpg';

imgArray[21] = new Image();
imgArray[21].src = 'img/onefuture.png';

imgArray[22] = new Image();
imgArray[22].src = 'img/greenwashing.png';

imgArray[23] = new Image();
imgArray[23].src = 'img/tucson.png';

imgArray[24] = new Image();
imgArray[24].src = 'img/WHObusconnects.png';

imgArray[25] = new Image();
imgArray[25].src = 'img/emissionscars.jpg';

imgArray[26] = new Image();
imgArray[26].src = 'img/builtforcars.webp';

imgArray[27] = new Image();
imgArray[27].src = 'img/5modes.png';

imgArray[28] = new Image();
imgArray[28].src = 'img/percentages.png';

imgArray[29] = new Image();
imgArray[29].src = 'img/cobaltmine.jpg';

imgArray[30] = new Image();
imgArray[30].src = 'img/shareofsuvs.png';

imgArray[31] = new Image();
imgArray[31].src = 'img/templeville2.png';

imgArray[32] = new Image();
imgArray[32].src = 'img/induceddemand.gif';

imgArray[33] = new Image();
imgArray[33].src = 'img/greenerontheroad.png';




// function newImage() {
//     var img = document.getElementById("lastImage");
//     for (var i = 0; i < imgArray.length; i++) {
//         if (imgArray[i].src == img.src) {
//             if (i === imgArray.length) {
//                 document.getElementById("lastImage").src = imgArray[0].src;
//                 break;
//             }
//             document.getElementById("lastImage").src = imgArray[i + 1].src;
//             break;
//         }
//     }
// }

const questions = [
    {
        question: 'What was the name of the first Bus Eireann mascot?',
        image: imgArray[0],
        answers: [
            { text: 'Tigger', correct: false },
            { text: 'Brad', correct: false },
            { text: 'Rusty', correct: false },
            { text: 'Rua', correct: true },


        ]
    },
    {
        question: 'Whose this fella?',
        image: imgArray[1],
        answers: [
            { text: 'Brendan Bus', correct: false },
            { text: 'Radial Rad', correct: false },
            { text: 'Trustworthy Trish', correct: false },
            { text: 'Network Noel', correct: true },


        ]
    },
    {
        question: 'Whats the longest Dublin bus route at approx 42km?',
        image: imgArray[2],
        answers: [
            { text: '17 to Blackrock', correct: false },
            { text: '33 to Balbriggan', correct: true },
            { text: '65 to Ballymore Eustace', correct: false },
            { text: '84 to Newcastle', correct: false },


        ]
    },


    {
        question: 'Now a little more serious....In Sep 2015, Volkswagen admitted to installing "defeat devices" in how many vehicles in order to cheat diesel emissions tests?',
        image: imgArray[3],
        answers: [
            { text: '50,000', correct: false },
            { text: '111,000', correct: false },
            { text: '5 million', correct: false },
            { text: '11 million', correct: true }
        ]
    },
    // {
    //     question: 'What was the biggest selling car brand in Ireland every year between 2015-2020?',
    //     image: imgArray[4],
    //     answers: [
    //         { text: 'Toyota', correct: false },
    //         { text: 'Renault', correct: false },
    //         { text: 'Hyundai', correct: false },
    //         { text: 'Volkswagen', correct: true },
    //     ]
    // },

    // {
    //     question: 'The Dublin Bus App does not include information on the approx 10% of routes licenced to Go-Ahead Ireland.In order to see a list of all bus routes, which app do you need to use?',
    //     image: imgArray[5],
    //     answers: [
    //         { text: 'TFI GO', correct: false },
    //         { text: 'Journey Planner', correct: false },
    //         { text: 'Real Time Ireland', correct: false },
    //         { text: 'There is no app that provides a full list of timetables', correct: true },
    //     ]
    // },
    // {
    //     question: 'Which one of the following 6 apps will give you the timetable for the 175 bus?',
    //     image: imgArray[6],
    //     answers: [
    //         { text: 'TFI Leap TopUp', correct: false },
    //         { text: 'Bus Eireann', correct: false },
    //         { text: 'TFI GO', correct: false },
    //         { text: 'Journey Planner', correct: true },
    //         { text: 'Real Time Ireland', correct: false },
    //         { text: 'Dublin Bus App', correct: false },
    //     ]
    // },
    // {
    //     question: 'The R112 is an orbital road that runs from Walkinstown beyond Dundrum that took the majority of Port and cross suburban traffic before the M50. How many orbital bus services will be run on the route after BusConnects?',
    //     image: imgArray[7],
    //     answers: [
    //         { text: '0', correct: true },
    //         { text: '2', correct: false },

    //     ]
    // },

    // // {
    // //     question: 'In 2011, under Labour Minister Alan Kelly, a new Aircoach service from Balinteer-Terenure-Dublin Airport was launched. What year was it pulled from service?',
    // //     image: imgArray[8],
    // //     answers: [
    // //         { text: '2013', correct: true },
    // //         { text: '2015', correct: false },
    // //         { text: '2017', correct: false },
    // //         { text: 'It never took off', correct: false },


    // //     ]
    // // },
    // {
    //     question: 'Despite successive years of strong economic growth, 2015 saw Irish Rail receive amongst it´s lowest recorded State Funding. Who was Transport Minister at the time?',
    //     image: imgArray[8],
    //     answers: [
    //         { text: 'Simon Harris (FG)', correct: false },
    //         { text: 'Mary Harney (PD)', correct: false },
    //         { text: 'Pascal Donohue (FG)', correct: true },
    //         { text: 'Joan Burton (Lab)', correct: false },

    //     ]
    // },
    // {
    //     question: 'Bus routes make limited usage of motorways in cities such as Ottawa and Madrid. Which expert group recommended some usage of the M50 by Dublin Bus back in 2018?',
    //     image: imgArray[9],
    //     answers: [
    //         { text: 'National Bus and Rail Union', correct: false },
    //         { text: 'The ESRI ', correct: false },
    //         { text: 'An Tasice', correct: true },
    //         { text: 'Dublin Chamber of Commerce', correct: false },
    //     ]
    // },
    // // {
    // //     question: 'On the M50, the legal limit for vehicles towing a trailer is 80km per hour, for HGVs it’s 90km per hour, what is the limit for ALL buses?',
    // //     image: imgArray[10],
    // //     answers: [
    // //         { text: '80kmph', correct: false },
    // //         { text: '65kmph', correct: false },
    // //         { text: '100kmph', correct: true },
    // //         { text: '50kmph', correct: false },
    // //     ]
    // // },

    // {
    //     question: 'According to a study from UCC, the number of private cars in Ireland increased by approximately 160% between 1990-2020. By what percentage did the population increase during this time?',
    //     image: imgArray[10],
    //     answers: [
    //         { text: '160%', correct: false },
    //         { text: '60%', correct: false },
    //         { text: '40%', correct: true },
    //         { text: '20%', correct: false },
    //     ]
    // },

    // {
    //     question: 'In many parts of Ireland, transport deprivation is correlated with forced car ownership and higher levels of car dependency. In 2019, what percentage of all journeys in Ireland were made in private cars?',
    //     image: imgArray[11],
    //     answers: [
    //         { text: '74%', correct: true },
    //         { text: '65%', correct: false },
    //         { text: '60%', correct: false },
    //         { text: '81%', correct: false },
    //     ]
    // },

    // {
    //     question: 'Apart from CO2 what other pollutants are released to varying levels from petrol and diesel cars?',
    //     image: imgArray[12],
    //     answers: [
    //         { text: 'NOx (oxides of nitrogen)', correct: false },
    //         { text: 'PM (particulate matter)', correct: false },
    //         { text: 'CO (carbon monoxide)   ', correct: false },
    //         { text: 'All of the above', correct: true },
    //     ]
    // },

    // {
    //     question: 'A 2018 report by the Committee on the Medical Effects of Air Pollutants COMEAP estimated the number of premature deaths in the UK figure as a result of air pollution. What was this figure?',
    //     image: imgArray[13],
    //     answers: [
    //         { text: '4000 deaths', correct: false },
    //         { text: '8500 deaths', correct: false },
    //         { text: '64000 deaths', correct: false },
    //         { text: '32000 deaths', correct: true },

    //     ]
    // },
    // // {
    // //     question: 'The change to an emissions-based taxation in 2008 was meant to encourage the purchase of cleaner cars? What happened to consumer buying habits?',
    // //     image: imgArray[9],
    // //     answers: [
    // //         { text: 'They bought smaller cars', correct: true },
    // //         { text: 'They bought diesel cars due to the lower CO2 emissions', correct: true },
    // //         { text: 'They bought bigger cars ', correct: false },
    // //         { text: '32000 deaths', correct: true },

    // //     ]
    // // },





    // {
    //     question: 'Which company has been the official sponsor for the Late Late Show on RTE since 2015?',
    //     image: imgArray[14],
    //     answers: [
    //         { text: 'Renault', correct: true },
    //         { text: 'Tayto', correct: false },
    //         { text: 'Glenisk', correct: false },
    //         { text: 'Hyundai', correct: false },

    //     ]
    // },

    // {
    //     question: 'In 2019, what percentage of journeys under 5km were made by private car in South Dublin?',
    //     image: imgArray[15],
    //     answers: [
    //         { text: '62%', correct: true },
    //         { text: '15%', correct: false },
    //         { text: '25%', correct: false },
    //         { text: '75%', correct: false },

    //     ]
    // },

    // {
    //     question: 'SUVs are reported to emit at least 14% more carbon dioxide than a small passenger car. What type car was Ireland´s most popular in 2020 and 2021?',
    //     image: imgArray[16],
    //     answers: [
    //         { text: 'SUV', correct: true },
    //         { text: 'Small Passenger Car', correct: false },
    //         { text: 'Hatchback', correct: false },
    //         { text: 'Sedan/Saloon', correct: false },
    //         // { text: 'Convertible', correct: false },


    //     ]
    // },

    // {
    //     question: 'Designing with the "we" in mind means what?',
    //     image: imgArray[17],
    //     answers: [
    //         { text: 'Sharing the road with your friends', correct: false },
    //         { text: 'Designing for maximum personal comfort', correct: false },
    //         { text: 'Thinking about how "I" can get there', correct: false },
    //         { text: 'Thinking about how "we" can get there', correct: true },



    //     ]
    // },
    // {
    //     question: 'Name the city marked in the barchart?',
    //     image: imgArray[18],
    //     answers: [
    //         { text: 'Bilbao', correct: false },
    //         { text: 'Galway', correct: true },
    //         { text: 'Oslo', correct: false },
    //         { text: 'Stockholm', correct: false },


    //     ]
    // },

    // {
    //     question: 'In 2011, Dublin ranked number 9 on the CopenHagen Index of Bicycle Friendly Cities for it´s work in improving it´s cycling grid. Where did it place in the latest 2019 polling?',
    //     image: imgArray[19],
    //     answers: [
    //         { text: '20th', correct: false },
    //         { text: '25th', correct: false },
    //         { text: '17th', correct: false },
    //         { text: 'It did not place', correct: true },


    //     ]
    // },

    // {
    //     image: imgArray[20],
    //     question: 'In what year did a member of the Green Party become the Minister for Transport?',


    //     answers: [
    //         { text: '2012', correct: false },
    //         { text: '2022', correct: false },
    //         { text: '2020', correct: true },
    //         { text: '2016', correct: false }
    //     ]
    // },
    // {
    //     image: imgArray[21],
    //     question: 'How can I see a party´s priorities on fighting climate change?',

    //     answers: [
    //         { text: 'What they say on Twitter', correct: false },
    //         { text: 'What they print on their brochure', correct: false },
    //         { text: 'How much they attack the opposition', correct: false },
    //         { text: 'Their annual budget, oireachtas.ie voting record ', correct: true }
    //     ]
    // },


    // {
    //     question: 'What is greenwashing?',
    //     image: imgArray[22],
    //     answers: [
    //         { text: 'disinformation intended to present an environmentally responsible public image.', correct: true },
    //         { text: 'taking meaningful action against climate change', correct: false },
    //         { text: 'treating customers fairly and ensuring that communications are clear', correct: false },
    //         { text: 'washing your clothes on the eco setting', correct: false },


    //     ]
    // },

    // {
    //     question: 'A car that has relatively low-moderate CO2 emissions and therefore taxed moderately can still emit high amounts of Carbon Monoxide or other toxic pollutants?',
    //     image: imgArray[23],
    //     answers: [
    //         { text: 'TRUE', correct: true },
    //         { text: 'FALSE', correct: false },



    //     ]
    // },

    // {
    //     question: 'In 1988 a report by the WHO concluded Diesel exhausts are probably carcinogenic to humans. In what year was this officially confirmed??',
    //     image: imgArray[24],
    //     answers: [
    //         { text: '1998', correct: false },
    //         { text: '2012', correct: true },
    //         { text: '2022', correct: false },
    //         { text: '1989', correct: false },


    //     ]
    // },
    // {
    //     question: 'Which of the following websites provides a more holistic emissions checker for your vehicle?',
    //     image: imgArray[25],
    //     answers: [
    //         { text: 'Trustpilot', correct: false },
    //         { text: 'Angies List', correct: false },
    //         { text: 'Which? ', correct: true },
    //         { text: 'Citysearch', correct: false },



    //     ]
    // },

    // {
    //     question: 'A design mock-up for an upcoming housing estate in Dublin. What percentage of new housing developments were built with active travel links in mind in 2020?',
    //     image: imgArray[26],
    //     answers: [
    //         { text: '56%', correct: false },
    //         { text: '5%', correct: false },
    //         { text: '14%', correct: true },
    //         { text: '68%', correct: false },
    //         // Hatchback
    //         // Sedan

    //     ]
    // },

    // {
    //     question: 'In the engineering study above which mode of transport made it across the finish line first (31 seconds)?',
    //     image: imgArray[27],
    //     answers: [
    //         { text: 'Pedestrians', correct: false },
    //         { text: 'Tram', correct: false },
    //         { text: 'Bus', correct: true },
    //         { text: 'Cyclists', correct: false },


    //     ]
    // },

    // {
    //     question: 'Ireland´s transport sector accounted for 20% of our annual emissions in 2019. What was this percentage in the USA?',
    //     image: imgArray[28],
    //     answers: [
    //         { text: '15%', correct: false },
    //         { text: '35%', correct: true },
    //         { text: '20%', correct: false },
    //         { text: '50%', correct: false },


    //     ]
    // },

    // {
    //     question: 'The minerals needed for Electric Vehicle batteries such as lithium and cobalt are mined. Cobalt, a toxic compound is mined primarily in which country?',
    //     image: imgArray[29],
    //     answers: [
    //         { text: 'Colombia', correct: false },
    //         { text: 'Bolivia', correct: false },
    //         { text: 'DRO Congo', correct: true },
    //         { text: 'Ethiopia', correct: false },


    //     ]
    // },
    // {
    //     question: 'In Ireland, half of the Top 10 best selling cars are SUV models as of 2021. What car type is the best seller in the UK?',
    //     image: imgArray[30],
    //     answers: [
    //         { text: 'SUV', correct: false },
    //         { text: 'Small Passenger Car', correct: true },
    //         { text: 'Hatchback', correct: false },
    //         { text: 'Sedan/Saloon', correct: false },
    //         // { text: 'Convertible', correct: false },


    //     ]
    // },
    // {
    //     question: 'Which reason is among the top reasons given by locals for objecting to new housing schemes in their local area?',
    //     image: imgArray[31],
    //     answers: [

    //         { text: 'Concerns it is not high density enough', correct: false },
    //         { text: 'Concerns about insufficient parking leading to residential overflow', correct: true },
    //         { text: 'Concerns there will be too many people taking their local bus', correct: false },
    //         { text: 'Concerns there will be not enough bicycle parking', correct: false },
    //         // { text: 'Convertible', correct: false },


    //     ]
    // },
    // {
    //     question: 'What is induced demand?',
    //     image: imgArray[32],
    //     answers: [

    //         { text: 'Traffic reduction from widening motorways', correct: false },
    //         { text: 'Demand that has been realised, or "generated", by improvements made to transportation infrastructure', correct: true },
    //         { text: 'Traffic that avoids a road/motorway due to fears of high traffic', correct: false },
    //         { text: 'The idea that building bigger roads will make traffic better', correct: false },
    //         // { text: 'Convertible', correct: false },


    //     ]
    // },
    // {
    //     question: 'In urban environments, public transport is more sustainable than electric vehicles because?',
    //     image: imgArray[33],
    //     answers: [

    //         { text: 'Gets more people more places in less time', correct: false },
    //         { text: 'EV´s take up as much roadspace as petrol/diesel cars', correct: false },
    //         { text: 'EV´s have a high up-front manufacturing footprint', correct: false },
    //         { text: 'All of the above', correct: true },
    //         // { text: 'Convertible', correct: false },


    //     ]
    // },







]